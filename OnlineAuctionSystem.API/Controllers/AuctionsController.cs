﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineAuctionSystem.AuctionService.Contracts.Interfaces;
using OnlineAuctionSystem.AuctionService.Contracts.Models;
using OnlineAuctionSystem.Entities.Exceptions;

namespace OnlineAuctionSystem.AuctionService.Controllers
{
    [Route("[controller]")]
    [Authorize]
    public class AuctionsController : BaseController
    {
        private readonly IAuctionsRepository _auctionsRepository;

        public AuctionsController(IAuctionsRepository auctionsRepository)
        {
            _auctionsRepository = auctionsRepository;
        }

        [HttpPost("new")]
        public async Task<JsonResult> AddNewAuction([FromBody] Auction auction)
        {
            if (auction == null)
            {
                var errorFields = GetModelStateErrorFields<Auction>();
                var error = new DeserializationException(errorFields);
                return error.ToJson();
            }

            auction.HolderId = CurrentUser.UserId;

            return new JsonResult(await _auctionsRepository.InsertAuctionAsync(auction));
        }

        [HttpGet("{auctionId:guid}")]
        public async Task<JsonResult> GetAuctionById(Guid auctionId)
        {
            var auction = await _auctionsRepository.GetUserAuctionByIdAsync(auctionId, CurrentUser.UserId);
            return auction == null ? new JsonResult(new StatusCodeResult(404)) : new JsonResult(auction);
        }
        
        [HttpGet]
        public async Task<JsonResult> GetAllUserAuctions()
        {
            return new JsonResult(await _auctionsRepository.GetAllAuctionsAsync(CurrentUser.UserId));
        }

        [AllowAnonymous]
        [HttpPost("search")]
        public async Task<JsonResult> SearchForAuctions([FromBody] SearchFilter filter)
        {
            if (filter == null)
            {
                var errorFields = GetModelStateErrorFields<SearchFilter>();
                var error = new DeserializationException(errorFields);
                return error.ToJson();
            }

            filter.UserId = CurrentUser?.UserId ?? 0;
            return new JsonResult(await _auctionsRepository.GetFilteredAuctionsAsync(filter));
        }
        
        [HttpDelete("{auctionId:guid}")]
        public async Task<JsonResult> DeleteAuction(Guid auctionId)
        {
            var result = await _auctionsRepository.DeleteAuctionAsync(auctionId, CurrentUser.UserId);
            return new JsonResult(result ? Ok() : new StatusCodeResult(404));
        }
        
        [HttpPost("{auctionId:guid}/edit")]
        public async Task<JsonResult> EditAuction([FromBody] Auction newAuction, Guid auctionId)
        {
            if (newAuction == null)
            {
                var errorFields = GetModelStateErrorFields<Auction>();
                var error = new DeserializationException(errorFields);
                return error.ToJson();
            }

            var editedAuction = await _auctionsRepository.EditAuctionAsync(newAuction, CurrentUser.UserId, auctionId);
            return editedAuction == null ? new JsonResult(new StatusCodeResult(404)) : new JsonResult(editedAuction);
        }

        [AllowAnonymous]
        [HttpGet("/internal/auctions")]
        public async Task<JsonResult> GetAuctionsByIdsInternal([FromQuery(Name="ids")] string ids)
        {
            var auction = await _auctionsRepository.GetAuctionsByIdInternalAsync(ids);
            return auction == null ? new JsonResult(new StatusCodeResult(404)) : new JsonResult(auction);
        }
    }
}