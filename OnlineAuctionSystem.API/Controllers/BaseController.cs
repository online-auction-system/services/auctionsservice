﻿using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using OnlineAuctionSystem.Authentication.MiddleWare;

namespace OnlineAuctionSystem.AuctionService.Controllers
{
    public class BaseController : Controller
    {
        protected OasIdentity CurrentUser
        {
            get
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return null;
                }

                return (OasIdentity) User.Identity;
            }
        }

        protected string GetModelStateErrorFields<T>()
        {
            if (ModelState.IsValid)
            {
                return string.Empty;
            }

            var test = ModelState.ErrorCount;
            var keys = ModelState.Keys.Where(k => ModelState[k].ValidationState == ModelValidationState.Invalid);
            var attribsDict = GetJsonPropertyAttributes<T>();
            var jsonPropFormattedNames =
                keys.Select(key => attribsDict.ContainsKey(key) ? attribsDict[key] : key).ToList();
            return string.Join(", ", jsonPropFormattedNames);
        }

        private static Dictionary<string, string> GetJsonPropertyAttributes<T>()
        {
            var res = new Dictionary<string, string>();

            var props = typeof(T).GetProperties();
            foreach (var prop in props)
            {
                var attrs = prop.GetCustomAttributes(true);
                foreach (var attr in attrs)
                {
                    if (attr is JsonPropertyAttribute jsonPropAttr)
                    {
                        var propName = prop.Name;
                        var jsonPropName = jsonPropAttr.PropertyName;

                        res.Add(propName, jsonPropName);
                    }
                }
            }

            return res;
        }
    }
}