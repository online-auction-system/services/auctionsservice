using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OnlineAuctionSystem.AuctionService.BuisnessLogic.Repositories;
using OnlineAuctionSystem.AuctionService.Contracts.Interfaces;
using OnlineAuctionSystem.AuctionService.DataAccess;
using OnlineAuctionSystem.AuctionService.DataAccess.Databases;
using OnlineAuctionSystem.Authentication.MiddleWare;
using OnlineAuctionSystem.DataAccessLibrary;
using OnlineAuctionSystem.ExceptionHandlerMiddleware;

namespace OnlineAuctionSystem.AuctionService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new SnakeCaseContractResolver();
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
            services.AddApiSecretAuth();
            services.AddSingleton<BaseDataAccess>();
            services.AddSingleton<IDatabaseConfig, DbConfig>();
            services.AddSingleton<IAuctionsDatabase, AuctionsDatabase>();
            services.AddSingleton<IAuctionsRepository, AuctionsRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseExceptionHandlerMiddleware();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}