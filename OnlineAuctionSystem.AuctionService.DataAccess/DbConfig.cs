﻿using OnlineAuctionSystem.DataAccessLibrary;

namespace OnlineAuctionSystem.AuctionService.DataAccess
{
    public class DbConfig : IDatabaseConfig
    {
        public int Timeout => 5;

        string IDatabaseConfig.DatabaseConfigurationString =>
            "Database=Online_Auction_System;Server=PROG-PC33;Integrated Security=False;uid=teamcity;pwd=teamcity;";
    }
}