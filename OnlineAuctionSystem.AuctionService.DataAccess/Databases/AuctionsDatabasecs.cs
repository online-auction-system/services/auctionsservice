﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using OnlineAuctionSystem.AuctionService.Contracts;
using OnlineAuctionSystem.AuctionService.Contracts.Interfaces;
using OnlineAuctionSystem.AuctionService.Contracts.Models;
using OnlineAuctionSystem.DataAccessLibrary;

namespace OnlineAuctionSystem.AuctionService.DataAccess.Databases
{
    public class AuctionsDatabase : IAuctionsDatabase
    {
        private readonly BaseDataAccess _dataAccess;

        public AuctionsDatabase(BaseDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public Task<Auction> InsertAuctionAsync(Auction auction)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Name", auction.Name);
            parameters.Add("@BargainTime", auction.BargainTime ?? Constants.DefaultAuctionBargainTime);
            parameters.Add("@EndTime", auction.EndTime);
            parameters.Add("@HolderId", auction.HolderId);
            parameters.Add("@MaxGoods", auction.MaxGoods ?? Constants.DefaultAuctionMaxGoods);
            parameters.Add("@MaxParticipants", auction.MaxParticipants ?? Constants.DefaultAuctionMaxParticipants);
            parameters.Add("@StartTime", auction.StartTime);
            parameters.Add("@MinBuyerReputation",
                auction.MinBuyerReputation ?? Constants.DefaultAuctionMinBuyerReputation);
            parameters.Add("@MinSellerReputation",
                auction.MinSellerReputation ?? Constants.DefaultAuctionMinSellerReputation);
            parameters.Add("@AllowAnonymous", auction.AllowAnonymous ?? true);
            return _dataAccess.ExecuteStoredProcedureWithSingleResultAsync<Auction>(StoredProcedures.InsertAuction,
                parameters);
        }

        public async Task<List<Auction>> GetAllAuctionsAsync(int userId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@UserId", userId);
            return (await _dataAccess.ExecuteStoredProcedureWithCollectionResultAsync<Auction>(
                StoredProcedures.GetAllAuctions,
                parameters)).ToList();
        }

        public async Task<List<Auction>> GetFilteredAuctionsAsync(SearchFilter filter)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@UserId", filter.UserId);
            parameters.Add("@UserRole", filter.UserRole);
            parameters.Add("@UserReputation", filter.UserReputation ?? Constants.DefaultAuctionMinBuyerReputation);
            parameters.Add("@AuctionStartTime", filter.AuctionStartTime);
            return (await _dataAccess.ExecuteStoredProcedureWithCollectionResultAsync<Auction>(
                StoredProcedures.GetFilteredAuctions,
                parameters)).ToList();
        }

        public async Task<List<Auction>> GetAuctionByIdsInternalAsync(IList<Guid> ids)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AuctionIds", MapIdsToJson(ids));
            return (await _dataAccess.ExecuteStoredProcedureWithCollectionResultAsync<Auction>(StoredProcedures.GetAuctionsById,
                parameters)).ToList();
        }

        public Task<bool> DeleteAuctionByIdAsync(Guid auctionId, int userId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AuctionId", auctionId);
            parameters.Add("@UserId", userId);
            return _dataAccess.ExecuteStoredProcedureWithSingleResultAsync<bool>(StoredProcedures.DeleteAuctionById,
                parameters);
        }

        public Task<Auction> EditAuctionAsync(Auction auction)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AuctionId", auction.AuctionId);
            parameters.Add("@Name", auction.Name);
            parameters.Add("@BargainTime", auction.BargainTime ?? Constants.DefaultAuctionBargainTime);
            parameters.Add("@EndTime", auction.EndTime);
            parameters.Add("@HolderId", auction.HolderId);
            parameters.Add("@MaxGoods", auction.MaxGoods ?? Constants.DefaultAuctionMaxGoods);
            parameters.Add("@MaxParticipants", auction.MaxParticipants ?? Constants.DefaultAuctionMaxParticipants);
            parameters.Add("@StartTime", auction.StartTime);
            parameters.Add("@MinBuyerReputation",
                auction.MinBuyerReputation ?? Constants.DefaultAuctionMinBuyerReputation);
            parameters.Add("@MinSellerReputation",
                auction.MinSellerReputation ?? Constants.DefaultAuctionMinSellerReputation);
            parameters.Add("@AllowAnonymous", auction.AllowAnonymous ?? true);
            return _dataAccess.ExecuteStoredProcedureWithSingleResultAsync<Auction>(StoredProcedures.EditAuction,
                parameters);
        }

        public Task<Auction> GetUserAuctionByIdAsync(Guid auctionId, int userId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AuctionId", auctionId);
            parameters.Add("@UserId", userId);
            return _dataAccess.ExecuteStoredProcedureWithSingleResultAsync<Auction>(StoredProcedures.GetUserAuctionById,
                parameters);
        }

        private static string MapIdsToJson(IList<Guid> ids)
        {
            return string.Join(",", ids);
        } 
    }
}