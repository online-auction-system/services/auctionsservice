﻿namespace OnlineAuctionSystem.AuctionService.DataAccess
{
    public static class StoredProcedures
    {
        public const string InsertAuction = "sp_Auction_Insert_v0";
        public const string EditAuction = "sp_Auction_Edit_v0";
        public const string GetAuctionsById = "sp_Auctions_Get_By_Ids_v0";
        public const string DeleteAuctionById = "sp_Auction_Delete_v0";
        public const string GetUserAuctionById = "sp_Auction_Get_With_Permissions_v0";
        public const string GetAllAuctions = "sp_Auctions_Get_v0";
        public const string GetFilteredAuctions = "sp_Auctions_Get_Filtered_v0";
    }
}