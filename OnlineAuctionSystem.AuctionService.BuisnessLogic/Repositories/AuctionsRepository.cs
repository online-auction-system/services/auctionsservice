﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using OnlineAuctionSystem.AuctionService.Contracts.Helpers;
using OnlineAuctionSystem.AuctionService.Contracts.Interfaces;
using OnlineAuctionSystem.AuctionService.Contracts.Models;
using OnlineAuctionSystem.Entities.Exceptions;

namespace OnlineAuctionSystem.AuctionService.BuisnessLogic.Repositories
{
    public class AuctionsRepository : IAuctionsRepository
    {
        private readonly IAuctionsDatabase _auctionsDatabase;

        public AuctionsRepository(IAuctionsDatabase auctionsDatabase)
        {
            _auctionsDatabase = auctionsDatabase;
        }

        public Task<Auction> InsertAuctionAsync(Auction auction)
        {
            if (auction.StartTime == default)
            {
                throw new JsonValidationException(new[]
                {
                    new ValidationFailure("start_time",
                        "Start time must be passed.")
                });
            }

            return _auctionsDatabase.InsertAuctionAsync(auction);
        }

        public Task<List<Auction>> GetAllAuctionsAsync(int userId)
        {
            return _auctionsDatabase.GetAllAuctionsAsync(userId);
        }

        public Task<List<Auction>> GetFilteredAuctionsAsync(SearchFilter filter)
        {
            if (filter.UserRole.HasValue)
            {
                if (!EnumExtensions.IsValidUserRole(filter.UserRole.Value))
                {
                    throw new JsonValidationException(new[]
                    {
                        new ValidationFailure("user_role",
                            "Value must be a valid user role or null")
                    });
                }
            }

            return _auctionsDatabase.GetFilteredAuctionsAsync(filter);
        }

        public Task<bool> DeleteAuctionAsync(Guid auctionId, int userId)
        {
            if (auctionId == Guid.Empty)
            {
                throw new JsonValidationException(new[]
                    {new ValidationFailure("auctionId", "Value must be non-default")});
            }

            return _auctionsDatabase.DeleteAuctionByIdAsync(auctionId, userId);
            //unbind all goods from auction 
        }

        public Task<Auction> EditAuctionAsync(Auction auction, int userId, Guid auctionId)
        {
            if (auctionId == Guid.Empty)
            {
                throw new JsonValidationException(new[]
                    {new ValidationFailure("auctionId", "Value must be non-default")});
            }

            if (auction.StartTime == default)
            {
                throw new JsonValidationException(new[]
                {
                    new ValidationFailure("start_time",
                        "Start time must be passed.")
                });
            }

            auction.HolderId = userId;
            auction.AuctionId = auctionId;

            return _auctionsDatabase.EditAuctionAsync(auction);
        }

        public Task<Auction> GetUserAuctionByIdAsync(Guid auctionId, int userId)
        {
            if (auctionId == Guid.Empty)
            {
                throw new JsonValidationException(new[]
                    {new ValidationFailure("auctionId", "Value must be non-default")});
            }

            return _auctionsDatabase.GetUserAuctionByIdAsync(auctionId, userId);
        }

        public Task<List<Auction>> GetAuctionsByIdInternalAsync(string ids)
        {
            var auctionIds = new List<Guid>();
            if (ids != null)
            {
                var split = ids.Split(',');
                foreach (var id in split)
                {
                    if (!Guid.TryParse(id, out var guidId))
                    {
                        throw new JsonValidationException(new[]
                            {new ValidationFailure("ids", "Non-guid ids can't be processed.")});
                    }

                    auctionIds.Add(guidId);
                }
            }

            return auctionIds.Count > 0
                ? _auctionsDatabase.GetAuctionByIdsInternalAsync(auctionIds)
                : Task.FromResult(new List<Auction>());
        }
    }
}