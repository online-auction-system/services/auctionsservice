﻿using System;
using OnlineAuctionSystem.AuctionService.Contracts.Enums;

namespace OnlineAuctionSystem.AuctionService.Contracts.Helpers
{
    public static class EnumExtensions
    {
        public static bool IsValidUserRole(int role)
        {
            return Enum.IsDefined(typeof(UserRoles), role) && role != (int) UserRoles.Unknown;
        }
    }
}