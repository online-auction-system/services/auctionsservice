﻿namespace OnlineAuctionSystem.AuctionService.Contracts
{
    public static class Constants
    {
        public const int DefaultAuctionBargainTime = 5;
        public const int DefaultAuctionMaxGoods = 100;
        public const int DefaultAuctionMaxParticipants = 100;
        public const int DefaultAuctionMinBuyerReputation = 0;
        public const int DefaultAuctionMinSellerReputation = 0;
    }
}