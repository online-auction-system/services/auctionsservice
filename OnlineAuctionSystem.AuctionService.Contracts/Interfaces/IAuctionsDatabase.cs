﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineAuctionSystem.AuctionService.Contracts.Models;

namespace OnlineAuctionSystem.AuctionService.Contracts.Interfaces
{
    public interface IAuctionsDatabase
    {
        Task<Auction> InsertAuctionAsync(Auction auction);
        Task<List<Auction>> GetAllAuctionsAsync(int userId);
        Task<List<Auction>> GetFilteredAuctionsAsync(SearchFilter filter);
        Task<List<Auction>> GetAuctionByIdsInternalAsync(IList<Guid> ids);
        Task<bool> DeleteAuctionByIdAsync(Guid auctionId, int userId);
        Task<Auction> EditAuctionAsync(Auction auction);
        Task<Auction> GetUserAuctionByIdAsync(Guid auctionId, int userId);
    }
}