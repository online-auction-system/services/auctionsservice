﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineAuctionSystem.AuctionService.Contracts.Models;

namespace OnlineAuctionSystem.AuctionService.Contracts.Interfaces
{
    public interface IAuctionsRepository
    {
        Task<Auction> InsertAuctionAsync(Auction auction);
        Task<List<Auction>> GetAllAuctionsAsync(int userId);
        Task<List<Auction>> GetFilteredAuctionsAsync(SearchFilter filter);
        Task<bool> DeleteAuctionAsync(Guid auctionId, int userId);
        Task<Auction> EditAuctionAsync(Auction auction, int userId, Guid auctionId);
        Task<Auction> GetUserAuctionByIdAsync(Guid auctionId, int userId);
        Task<List<Auction>> GetAuctionsByIdInternalAsync(string ids);
    }
}