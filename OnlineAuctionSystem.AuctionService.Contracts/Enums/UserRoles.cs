﻿namespace OnlineAuctionSystem.AuctionService.Contracts.Enums
{
    public enum UserRoles
    {
        Unknown = 0,
        Buyer = 1,
        Seller = 2
    }
}