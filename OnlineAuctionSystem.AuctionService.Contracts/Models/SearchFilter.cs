﻿using System;
using Newtonsoft.Json;

namespace OnlineAuctionSystem.AuctionService.Contracts.Models
{
    public class SearchFilter
    {
        [System.Text.Json.Serialization.JsonIgnore]
        public int UserId { get; set; }
        [JsonProperty("user_role")]
        public int? UserRole { get; set; }
        [JsonProperty("user_reputation")]
        public int? UserReputation { get; set; }
        [JsonProperty("auction_start_time")]
        public DateTime? AuctionStartTime { get; set; }
    }
}